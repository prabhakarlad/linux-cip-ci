#!/bin/bash
# Copyright (C) 2020, Renesas Electronics Europe GmbH, Chris Paterson <chris.paterson2@renesas.com>
# Copyright (C) 2020, Toshiba Nobuhiro Iwamtasu <nobuhiro1.iwamatsu@toshibac.co.jp>
#

set -e

# $1: OS type
# $2: Test name
create_job_definition () {
	local base_os="$1"
	local testname="$2"

	local dtb_url="$AWS_URL_DOWN/$DTB"
	local kernel_url="$AWS_URL_DOWN/$KERNEL"
	local modules_url="$AWS_URL_DOWN/$MODULES"

	local commit_ref=$(echo "${CI_COMMIT_REF_NAME}" | sed 's/\//-/g')

	if $USE_DTB; then
		local job_name="${commit_ref}_${VERSION}_${BUILD_ARCH}_${CONFIG}_${DTB_NAME}_${testname}"
	else
		local job_name="${commit_ref}_${VERSION}_${BUILD_ARCH}_${CONFIG}_${testname}"
	fi

	local job_definition="$TMP_DIR/${INDEX}_${job_name}.yaml"
	INDEX=$((INDEX+1))

	cat ${TEMPLATE_DIR}/header_base.yaml > "${job_definition}"
	if [ "${DEVICE}" = "qemu" ] ; then
		local device_name="${DEVICE}_${BUILD_ARCH}"
		cat ${TEMPLATE_DIR}/context_${device_name}.yaml >> "${job_definition}"
	else
		local device_name="${DEVICE}"
	fi

	cat ${TEMPLATE_DIR}/timeouts_"${testname}".yaml \
	    ${TEMPLATE_DIR}/action_"${base_os}"_"${device_name}".yaml \
	    ${TEMPLATE_DIR}/boot_"${base_os}"_"${device_name}".yaml \
	    ${TEMPLATE_DIR}/test_"${testname}".yaml \
	    >> "${job_definition}"

	sed -i "s|DEVICE_NAME|${DEVICE}|g" "$job_definition"
	sed -i "s|JOB_NAME|$job_name|g" "$job_definition"
	if $USE_MODULES; then
		sed -i "/DTB_URL/ a \    modules:\n      url: $modules_url\n      compression: gz" "$job_definition"
	fi
	if $USE_DTB; then
		sed -i "s|DTB_URL|$dtb_url|g" "$job_definition"
	fi
	sed -i "s|KERNEL_URL|$kernel_url|g" "$job_definition"

	echo ${job_definition}
	cat ${job_definition}
}

SCRIPT_DIR=$(cd $(dirname $0);pwd)

TEMPLATE_DIR="${SCRIPT_DIR}"
TMP_DIR="/tmp/"
AWS_URL_DOWN="exmample.org"
KERNEL="zImage"
DTB="foo.dtb"
MODULES="bar.tar.gz"
VERSION="4.19.1-1"
CI_COMMIT_REF_NAME="a/b/c/d"
CONFIG="testconfig"
DTB_NAME="test-dtb"
INDEX="1"
DEVICE=$1
BUILD_ARCH=$2
USE_DTB=$5

create_job_definition $3 $4
